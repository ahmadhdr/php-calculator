FROM php:7.3-fpm

WORKDIR /var/www/html

RUN apt-get update && apt-get install -y libpng-dev && apt-get -y install libzip-dev

RUN apt-get install -y \
    libwebp-dev \
    libjpeg62-turbo-dev \
    libpng-dev libxpm-dev \
    libfreetype6-dev

RUN docker-php-ext-configure gd \
    --with-gd \
    --with-webp-dir \
    --with-jpeg-dir \
    --with-png-dir \
    --with-zlib-dir \
    --with-xpm-dir \
    --with-freetype-dir

RUN apt-get install -y libpq-dev 

RUN docker-php-ext-install gd pdo pdo_mysql zip sockets bcmath