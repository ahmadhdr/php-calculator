<?php 

namespace Tests\Unit;

use Tests\TestCase;
use App\Services\CalculatorServiceImpl;
use App\Shared\EventProvider;
use Mockery;

class CalculatorTest extends TestCase {

    private $calculatorServiceImpl;
    private $mockEvent;

    public function setUp(): void {
        $this->calculatorServiceImpl = new CalculatorServiceImpl();
        $this->mockEvent = Mockery::mock(EventProvider::class);

    }

    public function testCalculatorServiceShouldInitializedBefore() 
    {
        $this->assertNotNull($this->calculatorServiceImpl);
    }

    public function testShouldCalculateNumbersAndReturn10() 
    {

        $data = [1,2,3,4];

        $expected = 10;

        $this->mockEvent
            ->shouldReceive("trigger")
            ->once();

        $response = $this->calculatorServiceImpl->add($data);

        $this->assertEquals($expected, $response);
        
    }

    public function testShouldCalculateNumbersAndReturn8() 
    {

        $data = [2,2,2,2];
        
        $expected = 8;

        $this->mockEvent
            ->shouldReceive("listen")
            ->once();


        $response = $this->calculatorServiceImpl->add($data);
        
        $this->assertEquals($expected, $response);
        
    }

    public function testShouldCalculateGivenNumbersAndReturnCorrectValue() 
    {
        $data = [100,1,200,300];
        
        $expected = 601;

        $this->mockEvent
            ->shouldReceive("listen")
            ->once();


        $response = $this->calculatorServiceImpl->add($data);
        
        $this->assertEquals($expected, $response);
    }

    public function testShouldInvalidArgumentExceptionBecauseGivenArgumentsIsNotValid() 
    {
        $data = [100];

        $response = $this->calculatorServiceImpl->add($data);

        $this->assertInstanceOf(\InvalidArgumentException::class,$response);
    }
    

    public function testShouldSubstractGivenNumbersAndReturn3() {
        $data = [10,4, 3];

        $expected = 3;

        $response = $this->calculatorServiceImpl->substract($data);

        $this->assertEquals($expected, $response);
        
    }

    public function testShouldSubstractGivenNumbersAndReturn5() {
        $data = [15,5,5];

        $expected = 5;

        $response = $this->calculatorServiceImpl->substract($data);

        $this->assertEquals($expected, $response);
        
    }

    public function testShouldSubstractGivenNumbersAndReturnCorrectValue() 
    {
        $data = [10,2,5];
        
        $expected = 3;

        $response = $this->calculatorServiceImpl->substract($data);
        
        $this->assertEquals($expected, $response);
    }
    


    public function testShouldCallSubstractMethodAndReturnInvalidArgumentExceptionBecauseGivenArgumentsIsNotValid() 
    {
        $data = [100];

        $response = $this->calculatorServiceImpl->substract($data);

        $this->assertInstanceOf(\InvalidArgumentException::class,$response);
    }

    public function testShouldMultiplyGivenNumbersAndReturnCorrectValue() 
    {
        $data = [3,4,7];
        
        $expected = 84;

        $response = $this->calculatorServiceImpl->multiply($data);
        
        $this->assertEquals($expected, $response);
    }


    public function testShouldDivideGivenNumbersAndReturnCorrectValue() 
    {
        $data = [50,2,5];
        
        $expected = 5;

        $response = $this->calculatorServiceImpl->divide($data);
        
        $this->assertEquals($expected, $response);
    }

    public function testShouldPowerGivenNumbersAndReturnCorrectValue() 
    {
        $data = [3,5];
        
        $expected = 243;

        $response = $this->calculatorServiceImpl->power($data);
        
        $this->assertEquals($expected, $response);
    }



}