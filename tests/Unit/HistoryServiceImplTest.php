<?php 

namespace Tests\Unit;

use Tests\TestCase;
use Mockery;
use App\Repositories\StorageRepository;
use App\Services\HistoryServiceImpl;
use App\Shared\ClientStorage;

class HistoryServiceImplTest extends TestCase {

    public $mockStorageRepository;
    public $historyServiceImpl;


    public function setUp(): void {
        parent::setUp();
        $this->mockStorageRepository = Mockery::mock(ClientStorage::class);
        $this->historyServiceImpl = new HistoryServiceImpl($this->mockStorageRepository);
    }  

    public function testShouldCallCreateHistoryAndValidateIfGivenCommandAvailable() {
        $this->mockStorageRepository
             ->shouldNotReceive("log")
            ->never();
        
        $callService = $this->historyServiceImpl->store("addding",null,null,null);


        $this->assertInstanceOf(\InvalidArgumentException::class,$callService);
    }

    public function testShouldCallCreateHistoryAndSuccess() {
        $operation = "1 + 2";
        $input = [1,2];
        $result = 3;
        $command = "add";

        $this->mockStorageRepository
            ->shouldReceive("write")
            ->atMost()

            ->times(1);

        $callService = $this->historyServiceImpl->store($command,$operation, $input, $result);

        $this->assertTrue($callService);

    }

    public function testShouldCallGetAllHistoryAndSuccess() {
        $this->mockStorageRepository
            ->shouldReceive("read")
            ->atMost()
            ->andReturn([])
            ->times(1);


        $callService = $this->historyServiceImpl->findAll();

        $this->assertIsArray($callService);

    }


    public function testShouldCallGetSpesificHistoryAndSuccess() {
        $this->mockStorageRepository
            ->shouldReceive("read")
            ->with(1)
            ->atMost()
            ->andReturn([])
            ->times(1);


        $callService = $this->historyServiceImpl->find(1);

        $this->assertIsArray($callService);

    }

    public function testShouldCallClearAndReturnTrue() {
        $this->mockStorageRepository
            ->shouldReceive("destroy")
            ->once();
        
        $callService = $this->historyServiceImpl->clear();
        
        $this->assertTrue($callService);

    }

    public function testShouldCallDestroyAndReturnTrue() {
        $this->mockStorageRepository
            ->shouldReceive("destroy")
            ->with(1)
            ->once();
        
        $callService = $this->historyServiceImpl->clear(1);
        
        $this->assertTrue($callService);

    }


}