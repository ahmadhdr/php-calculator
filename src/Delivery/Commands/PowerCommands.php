<?php

namespace App\Delivery\Commands;

use Illuminate\Console\Command;
use App\Services\CalculatorServiceImpl;

class PowerCommands extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "power {numbers*}";

    protected $calculatorServiceImpl;

    protected $historyServiceImpl;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This command is used to calculate the exponent of the given numbers, accepts only two arguments as its input (base, exponent)";

    public function __construct(
        CalculatorServiceImpl $calculatorServiceImpl
    ) {
        parent::__construct();

        $this->calculatorServiceImpl = $calculatorServiceImpl;

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arguments = $this->argument("numbers");

        $glue = sprintf(" %s ", "^");

        $desc = implode($glue, $arguments);

        $result = $this->calculatorServiceImpl->power($arguments);

        $this->comment(sprintf("%s = %s", $desc, $result));
    }
}
