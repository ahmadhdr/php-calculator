<?php

namespace App\Delivery\Commands;

use Illuminate\Console\Command;
use App\Services\HistoryServiceImpl;
use LucidFrame\Console\ConsoleTable;

class HistoryListByIdCommands extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "history:list {id} {--driver=}";

    protected $calculatorServiceImpl;

    protected $historyServiceImpl;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This command is used get history list";

    public function __construct(
        HistoryServiceImpl $historyServiceImpl
    ) {
        parent::__construct();

        $this->historyServiceImpl = $historyServiceImpl;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $option = $this->option("driver");

        $id = $this->argument("id");

        $driver = $this->option("driver");
        
        $this->historyServiceImpl->driver($driver);

        $history = $this->historyServiceImpl->find($id);
        if(count($history) > 0 ) {
            $table = new ConsoleTable();
            $table
            ->addHeader("ID")
            ->addHeader("Command")
            ->addHeader("Operation")
            ->addHeader("Result");

            foreach ($history as $each_history) {
                $table
                    ->addRow()
                    ->addColumn($each_history["id"])
                    ->addColumn($each_history["command"])
                    ->addColumn($each_history["operation"])
                    ->addColumn($each_history["result"]);
            }
            $table->display();
        }else {
            $this->comment("Data not found");
        }
        
        

    }
}
