<?php

namespace App\Delivery\Commands;

use Illuminate\Console\Command;
use App\Services\CalculatorServiceImpl;

class SubtractCommands extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "subtract {numbers*}";

    protected $calculatorServiceImpl;

    protected $historyServiceImpl;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This command is used to subtract all given numbers,  and accepts an endless number of inputs as its arguments";

    public function __construct(
        CalculatorServiceImpl $calculatorServiceImpl
    ) {
        parent::__construct();

        $this->calculatorServiceImpl = $calculatorServiceImpl;

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arguments = $this->argument("numbers");

        $glue = sprintf(" %s ", "-");

        $desc = implode($glue, $arguments);

        $result = $this->calculatorServiceImpl->substract($arguments);


        $this->comment(sprintf("%s = %s", $desc, $result));
    }
}
