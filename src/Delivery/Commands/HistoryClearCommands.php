<?php

namespace App\Delivery\Commands;

use Illuminate\Console\Command;
use App\Services\HistoryServiceImpl;

class HistoryClearCommands extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "history:clear {--id=}";

    protected $historyServiceImpl;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This command is used to clear spesific history ";

    public function __construct(
        HistoryServiceImpl $historyServiceImpl
    ) {
        parent::__construct();

        $this->historyServiceImpl = $historyServiceImpl;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id = $this->option("id");

        $this->historyServiceImpl->driver();

        $this->historyServiceImpl->clear($id);
        
        if(is_string($id)) {
            $this->comment("Data with ID ".$id. " is removed");
        } else {
            $this->comment("All history is cleared");
        }
    }
}
