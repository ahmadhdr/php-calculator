<?php

namespace App\Delivery\Commands;

use Illuminate\Console\Command;
use App\Services\CalculatorServiceImpl;
use App\Services\HistoryServiceImpl;

class MultiplyCommands extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "multiply {numbers*}";

    protected $calculatorServiceImpl;


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This command is used to multiply all given numbers,  and accepts an endless number of inputs as its arguments.";

    public function __construct(
        CalculatorServiceImpl $calculatorServiceImpl
    ) {
        parent::__construct();

        $this->calculatorServiceImpl = $calculatorServiceImpl;

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arguments = $this->argument("numbers");

        $glue = sprintf(" %s ", "*");

        $desc = implode($glue, $arguments);

        $result = $this->calculatorServiceImpl->multiply($arguments);


        $this->comment(sprintf("%s = %s", $desc, $result));
    }
}
