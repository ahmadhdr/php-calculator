<?php

namespace App\Delivery\Commands;

use Illuminate\Console\Command;
use App\Services\HistoryServiceImpl;
use LucidFrame\Console\ConsoleTable;

class HistoryListAllCommands extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "history:all {--driver=}";

    protected $historyServiceImpl;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This command is used get history list";

    public function __construct(
        HistoryServiceImpl $historyServiceImpl
    ) {
        parent::__construct();
        $this->historyServiceImpl = $historyServiceImpl;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $driver = $this->option("driver");
        
        $this->historyServiceImpl->driver($driver);


        $history = $this->historyServiceImpl->findAll();
        
        $table = new ConsoleTable();
        $table
            ->addHeader("ID")
            ->addHeader("Command")
            ->addHeader("Operation")
            ->addHeader("Result");

        foreach ($history as $file_driver) {
            $table
                ->addRow()
                ->addColumn($file_driver->id)
                ->addColumn($file_driver->command)
                ->addColumn($file_driver->operation)
                ->addColumn($file_driver->result);
        }
        $table->display();
    }
}
