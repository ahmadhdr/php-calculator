<?php

namespace App\Delivery\Http\Controller;

use Illuminate\Http\Request;
use App\Shared\CalculatorHelper;
use App\Services\CalculatorServiceImpl;
use App\Services\HistoryServiceImpl;
use App\Delivery\Http\Controller\Controller;

class CalculatorController extends Controller
{
    private $calculatorHelper;
    private $calculatorServiceImpl;
    private $historyServiceImpl;

    public function __construct(
        CalculatorHelper $calculatorHelper,
        CalculatorServiceImpl $calculatorServiceImpl,
        HistoryServiceImpl $historyServiceImpl
    ) {
        $this->calculatorHelper = $calculatorHelper;
        $this->calculatorServiceImpl = $calculatorServiceImpl;
        $this->historyServiceImpl = $historyServiceImpl;
    }

    public function calculate(Request $request, $action)
    {
        $numbers = $request->input("numbers");

        $result = 0;
        $operation = null;

        if ($action == "add") {
            $result = $this->calculatorServiceImpl->add($numbers);
            $operation = $this->calculatorHelper->getDescription($numbers, "+");
        } elseif ($action == "subtract") {
            $result = $this->calculatorServiceImpl->substract($numbers);
            $operation = $this->calculatorHelper->getDescription($numbers, "-");
        } elseif ($action == "divide") {
            $result = $this->calculatorServiceImpl->divide($numbers);
            $operation = $this->calculatorHelper->getDescription($numbers, "/");
        } elseif ($action == "multiply") {
            $result = $this->calculatorServiceImpl->multiply($numbers);
            $operation = $this->calculatorHelper->getDescription($numbers, "*");
        } elseif ($action == "power") {
            $result = $this->calculatorServiceImpl->power($numbers);
            $operation = $this->calculatorHelper->getDescription($numbers, "^");
        } else {
            return $this->httpNOK();
        }

        $payload = [
            "command" => $action,
            "operation" => $operation,
            "result" => $result,
        ];

        return $this->httpOK($payload);
    }
}
