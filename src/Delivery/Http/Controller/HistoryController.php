<?php

namespace App\Delivery\Http\Controller;

use App\Services\HistoryServiceImpl;
use Illuminate\Http\Request;
use App\Delivery\Http\Controller\Controller;

class HistoryController extends Controller
{
    private $historyServiceImpl;

    public function __construct(HistoryServiceImpl $historyServiceImpl)
    {
        $this->historyServiceImpl = $historyServiceImpl;
    }

    public function index(Request $request)
    {
        $driver = $request->input("driver");

        $this->historyServiceImpl->driver($driver);

        $callService = $this->historyServiceImpl->findAll();

        return $this->httpOK($callService);
    }

    public function show(Request $request, $id)
    {
        $driver = $request->input("driver");
        $this->historyServiceImpl->driver($driver);

        $callService = $this->historyServiceImpl->find($id);

        if (count($callService) < 1) {
            $payload = [
                "error" => true,
                "message" => "history with id " . $id . " is not found",
                "data" => null,
            ];

            return $this->httpNOK($payload);
        }

        return $this->httpOK($callService[0]);
    }

    public function remove(Request $request, $id)
    {
        $this->historyServiceImpl->driver("composite");

        $callService = $this->historyServiceImpl->clear($id);

        return $this->httpNOC();
    }
}
