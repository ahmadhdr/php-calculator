<?php

namespace App\Delivery\Http\Controller;
use Illuminate\Http\JsonResponse;

class Controller
{
    public function httpOK($data = [])
    {
        return new JsonResponse($data, 200);
    }

    public function httpNOK($data = [])
    {
        return new JsonResponse($data, 400);
    }

    public function httpNOC()
    {
        return new JsonResponse(null, 204);
    }
}
