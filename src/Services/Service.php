<?php
    
    namespace App\Services;

    use App\Shared\Event;

    class Service {

        public function dispatch($eventName, $data) {
            return Event::trigger($eventName, $data);            
        }

    }