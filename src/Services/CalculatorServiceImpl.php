<?php

namespace App\Services;

use App\Services\Service;
use App\Events\Calculator\{Added, Multiplied, Powered, Divided, Substracted};


class CalculatorServiceImpl extends Service
{
    public function add($numbers)
    {
        if (count($numbers) < 2) {
            return new \InvalidArgumentException("invalid arguments");
        }

        $result = 0;
        foreach ($numbers as $each_numbers) {
            $result += $each_numbers;
        }

        $this->dispatch(
            "added",
            new Added(
                [ "numbers" => $numbers, "result" => $result ]
            )
        );

        return $result;
    }

    public function substract($numbers)
    {
        if (count($numbers) < 2) {
            return new \InvalidArgumentException("invalid arguments");
        }

        $result = $numbers[0];

        for ($i = 1; $i < count($numbers); $i++) {
            $result -= $numbers[$i];
        }

        $this->dispatch(
            "substracted",
            new Substracted(
                [ "numbers" => $numbers, "result" => $result ]
            )
        );

        return $result;
    }

    public function multiply($numbers)
    {
        if (count($numbers) < 2) {
            return new \InvalidArgumentException("invalid arguments");
        }

        $result = $numbers[0];

        for ($i = 1; $i < count($numbers); $i++) {
            $result *= $numbers[$i];
        }

        $this->dispatch(
            "multiplied",
            new Multiplied(
                [ "numbers" => $numbers, "result" => $result ]
            )
        );

        return $result;
    }

    public function divide($numbers)
    {
        if (count($numbers) < 2) {
            return new \InvalidArgumentException("invalid arguments");
        }

        $result = $numbers[0];

        for ($i = 1; $i < count($numbers); $i++) {
            $result /= $numbers[$i];
        }

        $this->dispatch(
            "divided",
            new Divided(
                [ "numbers" => $numbers, "result" => $result ]
            )
        );

        return $result;
    }

    public function power($numbers)
    {

        if (count($numbers) == 2) {
            $result =  pow($numbers[0], $numbers[1]);

            $this->dispatch(
                "powered",
                new Powered(
                    [ "numbers" => $numbers, "result" => $result ]
                )
            );

            return $result;
        }
      

        return new \InvalidArgumentException("invalid arguments");
    }
}
