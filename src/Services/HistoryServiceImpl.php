<?php

namespace App\Services;

use App\Shared\ClientStorage;

class HistoryServiceImpl
{
    private $clientStorage;
    private $RANDOM_ID;

    public function __construct(ClientStorage $clientStorage)
    {
        $this->clientStorage = $clientStorage;

        $this->RANDOM_ID = rand(100, 1000000);
    }

    public function driver($driver = "") 
    {
        $this->clientStorage->setDriver($driver);
        return $this;
    }

    public function validateCommand($command)
    {
        $availableCommand = ["add", "subtract", "power", "divide", "multiply"];

        return in_array($command, $availableCommand);
    }

    public function store($command, $operation, $input, $result)
    {
        $response = $this->validateCommand($command);

        if (!$response) {
            return new \InvalidArgumentException("command notfound");
        }

        $payload = [
            "id" => $this->RANDOM_ID,
            "command" => $command,
            "operation" => $operation,
            "input" => $input,
            "result" => $result,
        ];

        $this->clientStorage->write($payload);

        return true;
    }

    public function find($id)
    {
        return $this->clientStorage->read($id);
    }

    public function findAll() 
    {
        return $this->clientStorage->read();
    }
    
    public function clear($id = null)
    {
        $this->clientStorage->destroy($id);

        return true;
    }
}
