<?php

namespace App\Providers;

use Illuminate\Contracts\Container\Container;
use App\Shared\ClientStorage;
use App\Shared\ClientStorageImpl;


class SharedServiceProvider
{
    public function register(Container $container): void
    {
        // bind storage repository to implementation
        $container->singleton(
            ClientStorage::class,
            ClientStorageImpl::class
        );
    }
}
