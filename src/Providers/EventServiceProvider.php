<?php

namespace App\Providers;

use Illuminate\Contracts\Container\Container;
use App\Shared\Event;
use App\Shared\CalculatorHelper;
use App\Services\HistoryServiceImpl;

class EventServiceProvider
{

    private $calculatorHelper;
    private $historyServiceImpl;

    public function __construct(CalculatorHelper $calculatorHelper, HistoryServiceImpl $historyServiceImpl)
    {
        $this->calculatorHelper = $calculatorHelper;
        $this->historyServiceImpl = $historyServiceImpl;
    }

    public function register(Container $container): void
    {
        Event::listen("added", function($param){
            $operation = $this->calculatorHelper->getDescription($param->data["numbers"], "+");


            $this->historyServiceImpl
                ->driver("")
                ->store("add",$operation,$param->data["numbers"], $param->data["result"] );
        });

        Event::listen("substracted", function($param){
            $operation = $this->calculatorHelper->getDescription($param->data["numbers"], "+");

            $this->historyServiceImpl
                ->driver("")
                ->store("subtract",$operation,$param->data["numbers"], $param->data["result"] );
        });

        Event::listen("multiplied", function($param){
            $operation = $this->calculatorHelper->getDescription($param->data["numbers"], "+");

            $this->historyServiceImpl
                ->driver("")
                ->store("multiply",$operation,$param->data["numbers"], $param->data["result"] );
        });

        Event::listen("powered", function($param){
            $operation = $this->calculatorHelper->getDescription($param->data["numbers"], "+");

            $this->historyServiceImpl
                ->driver("")
                ->store("power",$operation,$param->data["numbers"], $param->data["result"] );
        });

        Event::listen("divided", function($param){
            $operation = $this->calculatorHelper->getDescription($param->data["numbers"], "+");

            $this->historyServiceImpl
                ->driver("")
                ->store("divide",$operation,$param->data["numbers"], $param->data["result"] );
        });
    }
}
