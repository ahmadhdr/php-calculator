<?php

namespace App\Providers;

use Illuminate\Contracts\Container\Container;

use App\Repositories\StorageRepository;
use App\Repositories\Impl\FileRepositoryImpl;

class RepositoryServiceProvider
{
    public function register(Container $container): void
    {
        // bind storage repository to implementation
        $container->singleton(
            StorageRepository::class,
            FileRepositoryImpl::class
        );
    }
}
