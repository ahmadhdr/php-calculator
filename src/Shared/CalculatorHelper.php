<?php

namespace App\Shared;

class CalculatorHelper
{
    public function getDescription($number, $operator)
    {
        $glue = sprintf(" %s ", $operator);

        $desc = implode($glue, $number);

        return $desc;
    }
}
