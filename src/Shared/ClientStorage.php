<?php

namespace App\Shared;

interface ClientStorage {

    public function setDriver($driver = "");

    public function write($data);

    public function read($id = null);

    public function destroy($id = null);

}