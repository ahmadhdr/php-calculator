<?php 

namespace App\Shared;


interface EventProvider {

    public static function listen($name, $callback);
    public static function trigger($name, $argument = null);

}
