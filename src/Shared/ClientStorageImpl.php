<?php 

    namespace App\Shared;

    use App\Shared\ClientStorage;
    use App\Repositories\Impl\FileRepositoryImpl;
    use App\Repositories\Impl\LatestRepositoryImpl;
    
    class ClientStorageImpl implements ClientStorage 
    {

        protected $driver;

        public function setDriver($driver = "") 
        {
            if($driver == "file"){
                $this->driver = new FileRepositoryImpl();
            }elseif($driver == "latest"){
                $this->driver = new LatestRepositoryImpl();
            }else{
                $this->driver = [
                    new FileRepositoryImpl(),
                    new LatestRepositoryImpl()
                ];
            }
        }

        public function write($data) 
        {
            if(!is_array($this->driver)) {
                return 
                    $this->driver
                        ->log($data);
            }else {
                foreach($this->driver as $each_driver) {
                    $each_driver
                        ->log($data);
                }
            }
            
            
        }

        public function read($id = "")
        {
            if($id == "")  {
                if(!is_array($this->driver)) {
                    return $this->driver->findAll();
                }else {
                    $data = $this->driver[0]->findAll();
                    
                    return $data;
                }
            } else { 
                if(!is_array($this->driver)) {
                    return $this->driver->find($id);
                }else {
                     // check latest driver 
                     $latest_driver = $this->driver[1];
                     $result_from_latest_driver = $latest_driver->find($id);
 
                     if(count($result_from_latest_driver) > 0) {
                         return $result_from_latest_driver;
                     }else {
                          // check file driver
                         $file_driver = $this->driver[0];
                         $result_from_file_driver = $file_driver->find($id);
 
                         if(count($result_from_file_driver)) {
                             return $result_from_file_driver;
                         }
 
                         return [];
                     }
                }
            }
        }

        public function destroy($id = null)
        {
            if($id == null) {
                if(is_array($this->driver)) {
                    foreach($this->driver as $each_driver) {
                        $each_driver->clearAll();
                    }
                }
            }else {
                if(is_array($this->driver)) {
                    foreach($this->driver as $each_driver) {
                        $each_driver->clear($id);
                    }
                }
            }
        }
    }
