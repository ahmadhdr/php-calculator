<?php

namespace App\Repositories\Impl;

use App\Repositories\StorageRepository;
use Jajo\JSONDB;

class FileRepositoryImpl implements StorageRepository
{
    protected $storage;

    protected $fileName = "mesinhitung.json";

    public function __construct()
    {
        $this->storage = new JSONDB(__DIR__);
    }

    public function findAll()
    {
        return $this->storage
            ->select("*")
            ->from($this->fileName)
            ->get();
    }

    public function find($id)
    {
        return $this->storage
            ->select("*")
            ->from($this->fileName)
            ->where(["id" => $id])
            ->get();
    }

    public function log($command)
    {
        $randomId = rand(1262055681, 1262055681);

        return $this->storage->insert($this->fileName, $command);
    }

    public function clear($id)
    {
        return $this->storage
            ->delete()
            ->from($this->fileName)
            ->where(["id" => $id])
            ->trigger();
    }

    public function clearAll()
    {
        return $this->storage
            ->from($this->fileName)
            ->delete()
            ->trigger();
    }
}
