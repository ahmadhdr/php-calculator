<?php

namespace App\Repositories\Impl;

use App\Repositories\StorageRepository;
use Jajo\JSONDB;

class LatestRepositoryImpl implements StorageRepository
{
    protected $storage;

    protected $fileName = "latest.json";

    public function __construct()
    {
        $this->storage = new JSONDB(__DIR__);
    }

    public function findAll()
    {
        return $this->storage
            ->select("*")
            ->from($this->fileName)
            ->get();
    }

    public function find($id)
    {
        return $this->storage
            ->select("*")
            ->from($this->fileName)
            ->where(["id" => $id])
            ->get();
    }

    public function log($command)
    {
        if(count($this->findAll()) > 9) {
            $data = $this->findAll();
            
            $fistElement = $data[0];

            $this->clear($fistElement->id);

            return $this->storage->insert($this->fileName, $command); 

        }
        return $this->storage->insert($this->fileName, $command);
    }

    public function clear($id)
    {
        return $this->storage
            ->delete()
            ->from($this->fileName)
            ->where(["id" => $id])
            ->trigger();
    }

    public function clearAll()
    {
        return $this->storage
            ->from($this->fileName)
            ->delete()
            ->trigger();
    }
}
