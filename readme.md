<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->

<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">
    <img src="https://images.glints.com/unsafe/1024x0/glints-dashboard.s3.amazonaws.com/company-logo/0bfc3563efcb179435c27e250aa61048.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">PHP CALCULATOR</h3>

  <p align="center">This repository is the answer to the technical interview test given.
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li>
      <a href="#running-test"> Running Test </a>
    </li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

MesinHitung.id is a new startup that focuses on making a calculator that’s suitable for everyone.

### Built With
* [Docker](https://www.docker.com)
* [Laravel](https://laravel.com)
* [Nginx](https://nginx.com)
* [PHP Unit](https://phpunit.de/)


<!-- GETTING STARTED -->
## Getting Started

Before installing the project, you must first install Docker and git on your laptop.

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/ahmadhdr/php-calculator.git
   ```
2. Install libraries and software needed
   ```sh
   sh ./scripts/install.sh
   ```

### Running Test

1. Unit Test
   ```sh
   sh ./scripts/test.sh
   ```
2.  API Test
    
    Ensure that port 8080 is not used in your machine, after that, you can try this endpoint

    ```sh
    Get all history

    Method: GET
    http://localhost:8080/calculator?driver=file
    

    Get history by id
    
    Method: GET
    http://localhost:8080/calculator/{{yourId}}

    Calculate

    Method: POST
    Request body: {
      numbers: [1,2,3,4]
    }
    http://localhost:8080/calculator/{{command}}

    Delete history

    Method: Delete
    http://localhost:8080/calculator/{{historyID}}

    ```

<!-- CONTACT -->
## Contact

Email - ahmadhaidaralbaqir.official@gmail.com

Linkedin: [https://www.linkedin.com/in/ahmadhaidaralbaqir/](https://www.linkedin.com/in/ahmadhaidaralbaqir/)

