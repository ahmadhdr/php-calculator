#!/usr/bin/env bash

echo ""
echo "Running unit test..."
echo ""

 ./vendor/bin/phpunit --testdox

echo ""
echo "==== Running add command... ===="
echo ""
docker compose run --rm calculator add 1 3 5

echo ""
echo "==== Running subtract command ===";
echo ""
docker compose run --rm calculator subtract 10 2 5

echo ""
echo "==== Running multiply command ===";
echo ""
docker compose run --rm calculator multiply 3 4 7

echo ""
echo "==== Running divide command ===";
echo ""
docker compose run --rm calculator divide 50 2 5

echo ""
echo "==== Running power command ===";
echo ""
docker compose run --rm calculator power 3 5

echo ""
echo "==== Running get history all driver file ===";
echo ""
docker compose run --rm calculator history:all --driver=file

echo ""
echo "==== Running get history all driver latest ===";
echo ""
docker compose run --rm calculator history:all --driver=latest

echo ""
echo "==== Running get history all driver composite ===";
echo ""
docker compose run --rm calculator history:all

echo ""
echo "==== Running get history by id driver file (853578) ===";
echo ""
docker compose run --rm calculator history:list 853578 --driver=file

echo ""
echo "==== Running get history by id driver latest (853578) ===";
echo ""
docker compose run --rm calculator history:list 853578 --driver=latst

echo ""
echo "==== Running get history by id (853578) driver composite ===";
echo ""
docker compose run --rm calculator history:list 853578


echo ""
echo "==== Running clear history by id (218617) ===";
echo ""
docker compose run --rm calculator history:clear --id=853578


echo ""
echo "==== Running clear all history commands ===";
echo ""
docker compose run --rm calculator history:clear
