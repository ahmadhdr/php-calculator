#!/usr/bin/env bash

#!/bin/sh

echo "Installing php calculator project..."

echo ""
echo "=== Pulling and building docker image ==="
echo ""

docker-compose up -d --build

echo ""
echo "=== Installing composer depedencies ==="
echo ""

docker-compose run --rm composer install --ignore-platform-reqs

echo ""
echo "=== Finish ==="
echo ""
