<?php

use App\Providers\RepositoryServiceProvider;
use App\Providers\SharedServiceProvider;
use App\Providers\EventServiceProvider;

return [
    'providers' => [
        RepositoryServiceProvider::class,
        SharedServiceProvider::class,
        EventServiceProvider::class
    ],
];
