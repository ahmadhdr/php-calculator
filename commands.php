<?php

use App\Delivery\Commands\AddCommands;
use App\Delivery\Commands\SubtractCommands;
use App\Delivery\Commands\MultiplyCommands;
use App\Delivery\Commands\DivideCommands;
use App\Delivery\Commands\PowerCommands;
use App\Delivery\Commands\HistoryListAllCommands;
use App\Delivery\Commands\HistoryListByIdCommands;
use App\Delivery\Commands\HistoryClearCommands;

return [
    AddCommands::class,
    SubtractCommands::class,
    MultiplyCommands::class,
    DivideCommands::class,
    PowerCommands::class,
    HistoryListAllCommands::class,
    HistoryListByIdCommands::class,
    HistoryClearCommands::class
];
